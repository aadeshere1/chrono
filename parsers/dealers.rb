nokogiri = Nokogiri::HTML(content)

data = nokogiri.xpath('//script[@type="application/ld+json"]')
if data
	data = data&.text
end
data = JSON.parse(data)


offer_url = nokogiri.xpath('//a[contains(@class, "wt-current-offers")]/@href')

if offer_url
	offer_url = offer_url.text
end


dealer = {}

if homepage_text = nokogiri.at_css('#homepage-text h1')
	dealer["name"] = homepage_text.text
end

if country = nokogiri.at_css('.merchant-country')
	dealer["country"] = country.text
end

if website = nokogiri.at_css('.m-b-3 a.text-link')['href']
	dealer["website"] = website
end

if address = nokogiri.at_css('p.m-b-0')
	dealer["address"] = address.text.strip
end

if trusted_seller_since = nokogiri.xpath('//i[@class="i-trusted-seller"]/ancestor::div[contains(@class, "media")]//div[@class="media-body"]')
	dealer["trusted_seller_since"] = trusted_seller_since&.text.split.last.to_i
end

if recommendations = nokogiri.xpath('//div[contains(text(), "Number of recommendations")]').first
	dealer["recommendations"] = recommendations.text.split.last.to_i
end

dealer["has_store"] = nokogiri.xpath('//*[contains(text(), "owns a store")]').empty? ? false : true
dealer["phone"] = nokogiri.xpath('//div[contains(@class, "phone-fax-mobile")]//span[contains(@class,"text-nowrap")]').text.strip.gsub(/ +/, " ")
# dealer["phone"] = data["contactPoint"]["telephone"]
dealer["profile_url"] = data["url"]
dealer["image_url"] = data["logo"] #nokogiri.at_css('.merchant-logo')['src']

pages << {
		url: offer_url,
		fetch_type: 'browser',
		page_type: 'offers',
		force_fetch: true,
		vars: {
			dealer: dealer,
			trusted_checkouts: page['vars']['trusted_checkouts'],
			source: page['vars']['url']
			}
	}


	# cont = open(offers)
# doc = Nokogiri::HTML(cont)
# lsitings = doc.xpath('//div[contains(@class, "result-headline")]').text.split.first

# dealer["_collection"] = "dealers"

# outputs << dealer