nokogiri = Nokogiri::HTML(content)

list_count = nokogiri.xpath('//div[contains(@class, "result-headline")]').text.split.first.to_i

dealer = page['vars']['dealer']
checkouts = page['vars']['trusted_checkouts']

dealer["listings"] = list_count
dealer["trusted_checkouts"] = checkouts


dealer["_collection"] = "dealers"

outputs << dealer