nokogiri = Nokogiri::HTML(content)

dealers =  nokogiri.xpath('//div[contains(@class, "result-list")]/div[contains(@class, "media")]')

SITE_URL = "https://www.chrono24.ca".freeze

dealers.each do |dealer|
	link = dealer.xpath('.//div[@class="media-body"]//span[contains(@class, "m-t-0")]/a/@href')&.text
	url = SITE_URL + link
	checkouts = dealer.xpath('.//span[contains(@class, "trusted-checkout")]').text.strip.to_i
	if url =~ /\Ahttps?:\/\//i
		pages << {
			url: url,
			page_type: 'dealers',
			fetch_type: 'browser',
			force_fetch: true,
			vars: {
				category: page['vars']['category'],
				url: url,
				trusted_checkouts: checkouts,
				source: page['vars']['source']
			}
		}
	end
end

paginations = nokogiri.css('.pagination li')
page_range = paginations[1].text.strip.to_i..paginations[-2].text.strip.to_i
page_range.each do |page|
	# https://www.chrono24.ca/search/haendler.htm?LAND_ID=US&showpage=3
	url = "#{SITE_URL}/search/haendler.htm?LAND_ID=US&showpage=#{page}"
	pages << {
		url: url,
		page_type: 'listings',
		force_fetch: true,
		vars: {
			source: url
		}
	}
end